
module controller(IncA,IncB,wea,web,counter_rst,state,rst);

input[4:0] state;
input rst;
output IncA,IncB,wea,web,counter_rst;
wire IncA,IncB,wea,web,counter_rst;

and a0(wea0,~state[4],~state[3],~state[2],~state[1],~state[0]);
and a1(wea1,~state[4],~state[3],~state[2],~state[1],state[0]);
and a2(wea2,~state[4],~state[3],~state[2],state[1],~state[0]);
and a3(wea3,~state[4],~state[3],~state[2],state[1],state[0]);
and a4(wea4,~state[4],~state[3],state[2],~state[1],~state[0]);
and a5(wea5,~state[4],~state[3],state[2],~state[1],state[0]);
and a6(wea6,~state[4],~state[3],state[2],state[1],~state[0]);
and a7(wea7,~state[4],~state[3],state[2],state[1],state[0]);

or  o1(wea,wea0,wea1,wea2,wea3,wea4,wea5,wea6,wea7);	// wea

and na1(IncA1,state[4],~state[3],~state[2],~state[1],state[0]);
and na2(IncA2,state[4],~state[3],~state[2],state[1],~state[0]);
and na3(IncA3,state[4],~state[3],~state[2],state[1],state[0]);

nor no1(IncA,IncA1,IncA2,IncA3);			// IncA

and a12(IncB1,~state[4],state[3],~state[2],state[1],state[0]);
and a13(IncB2,~state[4],state[3],state[2],~state[1],state[0]);
and a14(IncB3,~state[4],state[3],state[2],state[1],state[0]);
and a15(IncB4,state[4],~state[3],~state[2],~state[1],state[0]);

or  o2(IncB,IncB1,IncB2,IncB3,IncB4);	// IncB

and a16(web1,~state[4],state[3],~state[2],state[1],~state[0]);
and a17(web2,~state[4],state[3],state[2],~state[1],~state[0]);
and a18(web3,~state[4],state[3],state[2],state[1],~state[0]);
and a19(web4,state[4],~state[3],~state[2],~state[1],~state[0]);

or  o3(web,web1,web2,web3,web4);		// web

and a20(state_reset,state[4],~state[3],~state[2],state[1],~state[0]);
or  o4(counter_rst, state_reset, rst);

endmodule