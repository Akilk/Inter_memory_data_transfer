
module state_counter(out, clk, counter_rst);

input clk;
input counter_rst;
output[4:0] out;

reg[4:0] out;

always @(posedge clk)
begin
	if(counter_rst)
	begin
		out = 5'b00000;
	end
	else
	begin
		out = out + 1;
	end
end

endmodule