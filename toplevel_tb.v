
module toplevel_tb();

reg[7:0] DataInA;
reg clk;
reg rst;

top_level main_module(.clk(clk),
					  .rst(rst),
					  .dataInA(DataInA));

initial 
begin
	rst = 1; clk = 0; DataInA = 0;
	//repeat(8) begin
		//@(posedge clk);
		//DataInA <= $random;	
	//	end
		
		@(posedge clk);
		DataInA <= 20;
		@(posedge clk);
		DataInA <= 10;
		@(posedge clk);
		DataInA <= 12;
		@(posedge clk);
		DataInA <= 5;
		@(posedge clk);
		DataInA <= 0;
		@(posedge clk);
		DataInA <= 5;
		@(posedge clk);
		DataInA <= 4;
		@(posedge clk);
		DataInA <= 25;
		
		
end

always
#5 clk = ~clk;

initial
begin
	#6 rst = 0;
end 

endmodule
