
module memory_B(
	DataInB,
	clk,
	web,
	AddrB
	);

// module inputs/outputs
input[7:0] DataInB;
input[1:0] AddrB;
input clk;
input web;

// signals coming/going from/to other modules
wire[7:0] DataInB;
wire[1:0] AddrB;
wire web;

reg[7:0] mem_B[3:0];

always @ (posedge clk)
begin
	if(web)
	begin
		mem_B[AddrB] <= DataInB;
	end
end

endmodule
