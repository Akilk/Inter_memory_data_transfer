
module alu_test();

reg clk;
reg[7:0] DOut1;
wire[7:0] DataInB;
wire[7:0] DOut2;

m2m_alu uut_test(.DataInB(DataInB),
				 .clk(clk),
				 .DOut1(DOut1),
				 .DOut2(DOut2));
				 
initial
begin
	clk = 0;
	forever #5 clk = ~clk;
end

initial
begin
	#10
	DOut1 = 8'h22;
	#10
	DOut1 = 8'h32;
	#10
	DOut1 = 8'h55;
	#10
	DOut1 = 8'h22;
	#10
	DOut1 = 8'h44;
	#10
	DOut1 = 8'h19;
	#10
	DOut1 = 8'h34;
end

endmodule