
module memory_A(
	DOut1,
	DataInA,
	clk,
	wea,
	AddrA
	);

// module inputs/outputs	
input[7:0] DataInA;
input[2:0] AddrA;
input clk;
input wea;
output[7:0] DOut1;

// signals coming/going from/to other modules
reg[7:0] DOut1;
wire[7:0] DataInA;
wire[2:0] AddrA;
wire wea;

reg[7:0] mem_A[7:0];

always @ (posedge clk)
begin
	if(wea)
	begin
		mem_A[AddrA] <= DataInA;
	end
	else 
	begin
		DOut1 <= mem_A[AddrA];
	end
end

endmodule
