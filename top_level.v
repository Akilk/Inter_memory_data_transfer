
module top_level(clk, rst, dataInA);

input rst;
input clk;
input[7:0] dataInA;

// inter module signals
wire[2:0] addrA;
wire IncB,IncA;
wire wea, web;
wire[7:0] DOut1;
wire[7:0] DOut2;
wire[7:0] dataInB, subOut, addOut;
wire[1:0] addrB;
wire[4:0] out;
wire counter_rst;
wire sign;

state_counter stcounter(.out(out),
	                    .clk(clk),
	                    .counter_rst(counter_rst));

controller control_unit(.IncA(IncA),
                        .IncB(IncB),
                        .wea(wea),
						.web(web),
                        .counter_rst(counter_rst),
						.state(out),
                        .rst(rst));

counter_A cA(.AddrA(addrA),
			 .rst(counter_rst),
			 .clk(clk),  
			 .IncA(IncA)  
			 );

memory_A mA(.DOut1(DOut1), 
			.DataInA(dataInA), 		
			.clk(clk),
			.wea(wea),
			.AddrA(addrA) 
			);

dflipflop dflipflop_Inst(.Q(DOut2),
						 .D(DOut1),
						 .clk(clk));

comparator comparator_Inst(.sign(sign), 
						   .Din1(DOut1),
						   .Din2(DOut2));

adder adder_Inst(.addOut(addOut), 
				 .DOut2(DOut2), 
				 .DOut1(DOut1));

subtracter subtracter_Inst(.subOut(subOut), 
						   .DOut2(DOut2), 
						   .DOut1(DOut1));

mux mux_Inst(.dataInB(dataInB),
			 .addOut(addOut), 
			 .subOut(subOut), 
			 .sign(sign));


counter_B cB(.AddrB(addrB), 
			 .rst(counter_rst),
			 .clk(clk),
			 .IncB(IncB));


memory_B mB(.DataInB(dataInB), 
			.clk(clk),
			.web(web),
			.AddrB(addrB));



endmodule