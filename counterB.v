
module counter_B(
	AddrB,
	rst,
	clk,
	IncB);
	
input rst;
input clk;
input IncB;
output[1:0] AddrB;

reg[1:0] AddrB;

always @ (posedge clk)
begin 
	if(rst)
	begin
		AddrB <= 2'h0;
	end
	else if (IncB)
	begin
		AddrB <= AddrB + 1;
	end
end

endmodule
