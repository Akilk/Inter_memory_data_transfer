
module counter_A(
	AddrA,
	rst,
	clk,
	IncA);
	
input rst;
input clk;
input IncA;
output[2:0] AddrA;

reg[2:0] AddrA;

always @ (posedge clk)
begin 
	if(rst)
	begin
		AddrA <= 3'b000;
	end
	else if (IncA)
	begin
		AddrA <= AddrA + 1;
	end
end

endmodule
